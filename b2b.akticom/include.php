<?
use Bitrix\Main\Loader;
use Bitrix\Main\EventManager;


$moduleId = "b2b.akticom";
$eventManager = EventManager::getInstance(); 

Loader::registerAutoLoadClasses(
    $moduleId,
    array(
        "B2BGroups"                         => "classes/main/B2BGroups.php",
        "B2BUsers"                          => "classes/main/B2BUsers.php",
        "B2BPrices"                         => "classes/main/B2BPrices.php",
        "B2BAkticom\\Prices\\PricesTable"   => "lib/price.php"

    )
);

$eventManager->registerEventHandler('main', 'OnEndBufferContent', $moduleId, 'B2BPrices', 'setPriceBuffer');
$eventManager->registerEventHandler('catalog', 'OnGetOptimalPrice', $moduleId, 'B2BPrices', 'getOptimalPriceHendler');
