<?
use Bitrix\Main\Application;
use Bitrix\Main\Loader;
use Bitrix\Main\Entity\Base;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ModuleManager;
use B2BAkticom\Prices\PricesTable;

Loc::loadMessages(__FILE__); 

class b2b_akticom extends CModule{

    var $MODULE_ID = "b2b.akticom";
    static private $_MODULE_ID = "b2b.akticom";
    var $MODULE_VERSION;
    var $MODULE_NAME;
    var $MODULE_DESCRIPTION;

    public function __construct()
    {
        $arModuleVersion = array();

        include __DIR__ . '/version.php';

        if (is_array($arModuleVersion) && array_key_exists('VERSION', $arModuleVersion)) {
            $this->MODULE_VERSION = $arModuleVersion['VERSION'];
            $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
        }

        $this->MODULE_ID = 'b2b.akticom';

        $this->MODULE_NAME = Loc::getMessage('B2B_MODULE_NAME');

        $this->MODULE_DESCRIPTION = Loc::getMessage('B2B_MODULE_DESCRIPTION');

        $this->MODULE_GROUP_RIGHTS = 'N';

        $this->PARTNER_NAME = Loc::getMessage('B2B_MODULE_PARTNER_NAME');
        $this->PARTNER_URI = 'b2b-module.dev';
    }

    public function isVersionD7(){
        return CheckVersion(\Bitrix\Main\ModuleManager::getVersion('main'), '14.00.00');
    }

    public function doInstall(){
        global $APPLICATION;

        if($this->isVersionD7())
        {
            \Bitrix\Main\ModuleManager::registerModule(self::$_MODULE_ID);
            if($this->InstallDB()){
                $tableName = PricesTable::getTableName();
                $sql = "CREATE INDEX b2b_index ON {$tableName} (PRODUCT_ID,GROUP_ID,PRICE)";
                Application::getConnection()->queryExecute($sql);
            }
            //$this->InstallData();
        }
        else
        {
            $APPLICATION->ThrowException(Loc::getMessage("B2B_PRICE_D7_INSTALL_ERROR_VERSION"));
        }

    }

    public function doUninstall(){
        $this->uninstallDB();
        ModuleManager::unRegisterModule($this->MODULE_ID);
    }

    public function installDB(){
        if (Loader::includeModule($this->MODULE_ID)) {
            PricesTable::getEntity()->createDbTable();
            return true;
        }
    }

    public function uninstallDB(){
        if (Loader::includeModule($this->MODULE_ID)) {
            if (Application::getConnection()->isTableExists(Base::getInstance('B2BAkticom\Prices\PricesTable')->getDBTableName())) {
                $connection = Application::getInstance()->getConnection();
                $connection->dropTable(PricesTable::getTableName());
            }
        }
    }
}
?>