<?php
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

$menu = array(
    array(
        'parent_menu' => 'global_menu_content',
        'sort' => 400,
        'text' => Loc::getMessage('B2B_MENU_TITLE'),
        'title' => Loc::getMessage('B2B_MENU_TITLE'),
        'url' => 'b2b_index.php',
        'items_id' => 'menu_references',
        'items' => array(
            array(
                'text' => Loc::getMessage('B2B_SUBMENU_TITLE'),
                'url' => 'b2b_index.php?lang=' . LANGUAGE_ID,
                'more_url' => array('B2B_index.php?lang=' . LANGUAGE_ID),
                'title' => Loc::getMessage('B2B_SUBMENU_TITLE'),
            ),
        ),
    ),
);

return $menu;