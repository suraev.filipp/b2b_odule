<?php
namespace B2BAkticom\Prices;

use Bitrix\Main\Entity\DataManager;
use Bitrix\Main\Entity\IntegerField;
use Bitrix\Main\Entity\StringField;
use Bitrix\Main\Entity\FloatField;
use Bitrix\Main\Entity\Validator;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Type;
Loc::loadMessages(__FILE__);

class PricesTable extends DataManager
{
    // название таблицы
    public static function getTableName()
    {
        return 'b2b_prices_table';
    }
    // создаем поля таблицы
    public static function getMap()
    {
        return array(
            new IntegerField('ID', array(
                'autocomplete' => true,
                'primary' => true
            )),// autocomplite с первичным ключом
            new IntegerField('PRODUCT_ID', array(
                'required' => true,
                'title' => Loc::getMessage('B2B_PRODUCT_ID')
            )),
            new IntegerField('GROUP_ID', array(
                'required' => false,
                'title' => Loc::getMessage('B2B_GROUP_ID')           
            )),
            new StringField('PRODUCT_XML_ID', array(
                'required' => false,
                'title' => Loc::getMessage('B2B_PRODUCT_XML_ID'),
                'validation' => function () {
                    return array(
                        new Validator\Length(null, 255),
                    );
                }              
            )),
            new StringField('GROUP_XML_ID', array(
                'required' => false,
                'title' => Loc::getMessage('B2B_GROUP_XML_ID'),
                'validation' => function () {
                    return array(
                        new Validator\Length(null, 255),
                    );
                }              
            )),
            new FloatField('PRICE', array(
                'required' => false,
                'title' => Loc::getMessage('B2B_PRICE'),             
            )),
        );
    }
}