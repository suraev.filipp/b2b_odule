<?

class B2BUserValidate
{

    public static function OnBeforeUserUpdateHandler(&$arFields)
    {

        global $APPLICATION;

        try{
            $DEUser = DEUser::createByID(intval($arFields['ID']));

            if($DEUser->isB2BUser()){
                self::validateCompany($arFields);
                self::validatePhone($arFields);
                self::validateKpp($arFields);
                self::validateINN($arFields);
            }
        }
        catch (Exception $exception){
            $APPLICATION->throwException($exception->getMessage());
            return false;
        }
    }

    /**
     * @throws Exception
     */
    protected static function validateCompany ($arFields)
    {
        if(is_set($arFields, "WORK_COMPANY") && strlen($arFields["WORK_COMPANY"])<=0){
            throw new Exception('Поле Организация обязательно для заполнения');
        }
    }

    /**
     * @throws Exception
     */
    protected static function validatePhone ($arFields){

        if(is_set($arFields, 'WORK_PHONE')){
            if (empty($arFields['WORK_PHONE'])){
                throw new Exception("Не указан номер телефона");
            }

            if (!preg_match('#^((\+7|7|8)+(\d){10})$#', $arFields['WORK_PHONE'])) {
                throw new Exception("Номер телефона указан в неправильном формате");
            }
        }

    }

    /**
     * @throws Exception
     */
    protected static function validateINN ($arFields){
        if(is_set($arFields, 'UF_INN')){
            if (empty($arFields['UF_INN'])) {
                throw new Exception( 'Не заполнен ИНН');
            }
            elseif (preg_match('/[^0-9]/', $arFields['UF_INN'])) {
                throw new Exception('ИНН может состоять только из цифр');
            }
            elseif (!in_array(
                $inn_length = strlen($arFields['UF_INN']),
                [
                    10,
                    12
                ]
            )) {
                throw new Exception( 'ИНН может состоять только из 10 или 12 цифр');
            }
            else {
                $result = false;

                $check_digit = function ($inn, $coefficients) {
                    $n = 0;
                    foreach ($coefficients as $i => $k) {
                        $n += $k * (int)$inn[$i];
                    }
                    return $n % 11 % 10;
                };
                switch ($inn_length) {
                    case 10:
                        $n10 = $check_digit(
                            $arFields['UF_INN'],
                            [
                                2,
                                4,
                                10,
                                3,
                                5,
                                9,
                                4,
                                6,
                                8
                            ]
                        );
                        if ($n10 === (int)$arFields['UF_INN'][9]) {
                            $result = true;
                        }
                        break;
                    case 12:
                        $n11 = $check_digit(
                            $arFields['UF_INN'],
                            [
                                7,
                                2,
                                4,
                                10,
                                3,
                                5,
                                9,
                                4,
                                6,
                                8
                            ]
                        );
                        $n12 = $check_digit(
                            $arFields['UF_INN'],
                            [
                                3,
                                7,
                                2,
                                4,
                                10,
                                3,
                                5,
                                9,
                                4,
                                6,
                                8
                            ]
                        );
                        if (($n11 === (int)$arFields['UF_INN'][10]) && ($n12 === (int)$arFields['UF_INN'][11])) {
                            $result = true;
                        }
                        break;
                }

                if (!$result) {
                    throw new Exception( 'Неправильное контрольное число');
                }
            }
        }
    }

    /**
     * @throws Exception
     */
    protected static function validateKpp ($arFields){
        if(is_set($arFields, 'UF_KPP')){
            if (strlen($arFields['UF_KPP']) !== 9) {
                throw new Exception('КПП может состоять только из 9 знаков');
            }
            elseif (!preg_match('/^[0-9]{4}[0-9A-Z]{2}[0-9]{3}$/', $arFields['UF_KPP'])) {
                throw new Exception('Неправильный формат КПП');
            }
        }
    }

}

?>