<?
use \Bitrix\Main\Type,
\Bitrix\Main\Config\Option,
\Bitrix\Main\Loader,
\Bitrix\Main\Localization\Loc;

class B2BUsers {

    public static function updateUser(){

    }

    public static function deleteUser(){
        
    }

    public static function addUser(){
        
    }

    public static function getUserIdByXml(){
        
    }
    
    static function isB2BUser($id = false){
        global $USER;
        $order = array('sort' => 'asc');
        $tmp = 'sort';
        $filter = array('ID' => $USER->GetID());
        $select = array("SELECT" => array("UF_IS_B2B_USER", "UF_B2B_CONFIRMED"));
        $rsUsers = CUser::GetList($order, $tmp, $filter, $select);
        if ($arUser = $rsUsers->Fetch()) {
            $UF_IS_B2B_USER = $arUser['UF_IS_B2B_USER'];
            $UF_B2B_CONFIRMED = $arUser['UF_B2B_CONFIRMED'];
        }
        if($UF_IS_B2B_USER && $UF_B2B_CONFIRMED){
            return true;
        }
        return false;
    }

    static function setSessionGroups(){
        global $USER;
        if($USER->IsAuthorized()){
            $session = Application::getInstance()->getSession();
            $user_id = $USER->GetID();
            if(!$session->has("session_groups_array")){
                $groupsArray = $USER->GetUserGroupArray();
                $groupsString = implode(",",$groupsArray);
                $session->set('session_groups_array', $groupsString);
                $GLOBALS['session_groups_array'] = $groupsArray;
            }else{
                $groupsString = $session->get('session_groups_array');
                $groupsArray = explode(",",$groupsString);
                $GLOBALS['session_groups_array'] = $groupsArray;
            }
        }else{

        }
    }
}
?>