<?
use \Bitrix\Main\Type,
\Bitrix\Main\Config\Option,
\Bitrix\Main\Loader,
\Bitrix\Main\Localization\Loc,
\B2BAkticom\PricesTable\PricesTable,
B2BUsers;


class B2BPrices {
    
    public static function getPlaceHolder($id,$price){
        return '#B2B_PRICE_'.$id.'_'.$price.'#';
    }

    public static function getOptimalPrice($product_id,$price){
        $isB2B = B2BUsers::isB2BUser();
        $isB2B = false;
        if(!$isB2B){
            return $price;
        }
        $filter_array = $GLOBALS['session_groups_array'];
        $result_price = floatval($price);
        $hlbl_id = self::$HL_block_id;

        $rsData = PricesTable::getList(array(
            "select" => array("*"),
            "order" => array("PRICE" => "ASC"),
            "filter" => array("=PRODUCT_ID"=>$product_id, "=GROUP_ID" => $filter_array),
            'limit' => 1
        ));
        
        if($arData = $rsData->Fetch()){
            $b2b_price = floatval($arData["UF_PRICE"]);
            if($result_price > $b2b_price){
                $result_price = $b2b_price;
            }else{

            }
        }else{

        }
        return $result_price;
    }

    public static function getOptimalPriceHendler($productID, $quantity = 1, $arUserGroups = array(), $renewal = "N", $arPrices = array(), $siteID = false, $arDiscountCoupons = false){
        $isB2B = B2BUsers::isB2BUser();

        if(!$isB2B){
            return true;
        }

        $minPriceArray = array();
        $dbProductPrice = CPrice::GetListEx(
            array(),
            array("PRODUCT_ID" => $productID),
            false,
            false,
            array("ID", "CATALOG_GROUP_ID", "PRICE", "CURRENCY", "QUANTITY_FROM", "QUANTITY_TO")
        );

        while($priceArray = $dbProductPrice->Fetch()){
            $minPriceArray[] = floatval($priceArray["PRICE"]);
    
        }

        $minPrice = min($minPriceArray);
        $b2bPrice = B2bPrices::getOptimalPrice($productID,$minPrice);
    
        return array(
            'PRICE' => array(
                "ID" => $productID,
                'CATALOG_GROUP_ID' => $catalog_group_id,
                'PRICE' => $b2bPrice,
                'CURRENCY' => "RUB",
                'ELEMENT_IBLOCK_ID' => $productID,
                'VAT_INCLUDED' => "Y",
            ),
            'DISCOUNT' => array(
                'VALUE' => 0,
                'CURRENCY' => "RUB",
            ),
        );
    }    

    public static function setPriceBuffer(&$content){
        
        global $APPLICATION;
        if (strpos( $APPLICATION->GetCurDir() , '/bitrix/admin/' ) === false){
            $pattern = '/\#B2B\_PRICE\_([0-9]+)\_([0-9.]+)\#/';
            preg_match_all($pattern, $content, $matches);
            $replaceArr = array();
            foreach($matches[0] as $key => $item){
                $placeholder =  $item;
                $prod_id = $matches[1][$key];
                $replaceArr[$placeholder] = self::getOptimalPrice($matches[1][$key] ,$matches[2][$key]);
    
            }
            $content = str_replace(array_keys($replaceArr), array_values($replaceArr), $content);
        }
    }
}
?>